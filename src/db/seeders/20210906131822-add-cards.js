'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('cards',[
      {
        brand: 'MasterCard',
        stripe_id: 'card_1JbYNkDMvxQ5xDjvTLoMt0Se',
        user_id: 1,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        brand: 'MasterCard',
        stripe_id: 'card_1JbYOaDMvxQ5xDjv23vjBkZc',
        user_id: 2,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('cards', null, {});
  }
};
