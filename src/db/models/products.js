'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class products extends Model {
    static associate(models) {
      products.hasOne(models.transactions, {
        foreignKey: "product_id"
      })
      // define association here
    }
  };
  products.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'products',
    underscored: true,
  });
  return products;
};