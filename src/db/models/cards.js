'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cards extends Model {
    static associate(models) {
      // define association here
      cards.belongsTo(models.users)
    }
  }
  cards.init({
    brand: DataTypes.STRING,
    stripe_id: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'cards',
    underscored: true,
  });
  return cards;
};