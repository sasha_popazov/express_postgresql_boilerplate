const {getUsers,getUserById, createUser,getByEmail} = require("../user/services")


jest.mock('sequelize');
jest.mock('../db/models', () => ({
    users: {
        findAll: jest.fn(),
        findOne: jest.fn().mockReturnValue({}),
        create: jest.fn().mockReturnValue({ dataValues: { password: 'test_password', email: 'test@gmail.com' } }),
    }
}));

const {users} = require("../db/models")

describe('Testing user service', () => {
    describe('Get all users', () => {
        test('Should call findAll method', async () => {

            await getUsers();
            expect(users.findAll).toHaveBeenCalled();
        });
    });

    describe('Get some user', () => {
        test('Should call findAll method', async () => {

            const id = 1

            await getUserById(id);
            expect(users.findOne).toHaveBeenCalledWith({where: { id } });
        });
    });

    describe('Create new user', () => {
        test('Should call create method', async () => {

            const user = { email: 'test@gmail.com' }
            await createUser(user);
            expect(users.create).toHaveBeenCalledWith(user);
        });
    });

    describe('Get user by email', () => {
        test('Should call findOne method', async () => {

            const email = 'test@gmail.com';

            await getByEmail(email);
            expect(users.findOne).toHaveBeenCalledWith({ where: { email } });
        });
    });
});
