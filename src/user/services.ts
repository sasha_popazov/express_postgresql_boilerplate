const { users } = require("../db/models");

export async function getUsers() {
  try {
    const { dataValues } = await users.findAll();
    return dataValues;
  } catch (err) {
    console.error('Table "users" is empty!');
  }
}

export async function getUserById(id: any) {
  try {
    const { dataValues } = await users.findOne({ where: { id } });
    return dataValues;
  } catch (err) {
    console.error("This user does not exist!");
  }
}

export async function createUser(user: any) {
  try {
    const { dataValues } = await users.create({ ...user });
    return dataValues;
  } catch (err) {
    console.error("User not created");
  }
}

export async function getByEmail(email: any) {
  try {
    const data = await users.findOne({ where: { email } });
    return data;
  } catch (err) {
    console.error("Users email not found");
  }
}

export async function updateUser(user: any) {
  try {
    const data = await users.update(user, { where: { id: user.id } });
    return data;
  } catch (err) {
    console.error("Customer id not updated");
  }
}


