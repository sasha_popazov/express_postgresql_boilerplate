const {cards, transactions } = require("../db/models");

export async function getPaymentMethod(userID: any) {
  try {
    const card = await cards.findOne({ where: { user_id: userID } });
    return card
  } catch (err) {
    console.error("Payment method not got!");
  }
}

export async function createPayment(card: any) {
  try {
    const cardObj = await cards.create(card);
    return cardObj;
  } catch (err) {
    console.error("Payment card not created");
  }
}

export async function generateTransaction(data: any) {
  try {
    const testTransaction = await transactions.create(data);
    return testTransaction;
  } catch (err) {
    console.error("Transaction not created");
  }
}
