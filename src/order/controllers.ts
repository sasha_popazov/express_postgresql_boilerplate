import express from "express";
import {
  generateTransaction,
  getPaymentMethod,
  createPayment,
} from "./services";
import { getUserById, updateUser} from "../user/services";
const stripe = require("stripe")(process.env.SECRET_KEY);

export async function handlePayment(
  req: express.Request,
  res: express.Response
) {
  const { token_id, user_id, amount, product_id } = req.body;

  let users = await getUserById(user_id);

  if (users && !users.customer_id) {
    const customer = await stripe.customers.create({
      email: users.email,
      name: `${users.first_name} ${users.last_name}`,
    });
    users.customer_id = customer.id
    await updateUser({ ...users });
  }

  let cards = await getPaymentMethod(user_id);

  if (!cards) {
    const source = await stripe.customers.createSource(
      users.customer_id,
      {source: token_id});

    cards = await createPayment({
      brand: "MasterCard",
      stripe_id: source.id,
      user_id: users.id,
    });
  }

  const charge = await stripe.charges.create({
    source: cards.stripe_id,
    amount: 3500,
    currency: "usd",
    description: `User_id: ${user_id} paid $${amount} for order - ${product_id}!`,
    customer: users.customer_id,
  });

  await generateTransaction(req.body);

  res.send(charge);
  res.end();
}
