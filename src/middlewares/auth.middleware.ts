import { createUser, getByEmail } from "../user/services";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import express from "express";

const { secret } = require("../config/server");

export async function registerUser(
  req: express.Request,
  res: express.Response
) {
  const newUser = await createUser(req.body);
  res.json(newUser);
  res.status(201);
}

export async function loginUser(req: express.Request, res: express.Response) {
  const jwtLifespan = 60 * 60;

  const { id, first_name, last_name, email, password } = await getByEmail(
    req.body.email
  );

  const match = await bcrypt.compare(req.body.password, password);

  if (match) {
    const token = jwt.sign(
      {
        subject: { id, first_name, last_name, email },
        exp: Math.floor(Date.now() / 1000) + jwtLifespan,
      },
      secret
    );
    res.send({ accessToken: token });
  } else {
    console.error("Password is incorrect!");
    res.status(404);
  }
  res.status(200);
  res.end();
}
