import fs from "fs";
import express from "express";
import util from "util";

const demoLogger = (
  logFile: fs.WriteStream,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  let current_datetime = new Date();
  let formatted_date =
    current_datetime.getFullYear() +
    "-" +
    (current_datetime.getMonth() + 1) +
    "-" +
    current_datetime.getDate() +
    " " +
    (current_datetime.getHours() + 3) +
    ":" +
    current_datetime.getMinutes();

  logFile.write(
    util.format("\n" + "Request data: " + `${formatted_date}`) + "\n"
  );
  logFile.write(util.format("Request url: " + req.url) + "\n");
  logFile.write(util.format("Request method: " + req.method) + "\n");
  logFile.write(util.format("Request status code: " + res.statusCode) + "\n");
  next();
};

export default demoLogger;
