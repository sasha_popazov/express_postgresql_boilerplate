
import React from 'react';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import PaymentContainer from '../containers/payment';
const stripe = loadStripe('pk_test_51JY7V0DMvxQ5xDjvme8MeVRjn19BAYDxyXS3X0jrvY457gg8cmLA3H4la3x6PrcSPfpNFsfrdXiJWeD5fXP5shyq00xzV4uXE3')
export default function PaymentPage() {
  return <Elements stripe={stripe}>
    <PaymentContainer/>
  </Elements>
}
