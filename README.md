# Express-Sequelize-PostgreSQL boilerplate

## Start serverExpress + postgresDatabase + frontStripe + frontUpload

## FOR DEV
```
docker-compose -f docker-compose.dev.yml build
docker-compose -f docker-compose.dev.yml up
```

## FOR PROD
```
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up
```

## Starter initialization

### Step one. Initializing package.json, git.

```
- npm init (create package.json)
- git init (create git repository)
```

### Step two. Installing the required dependencies.

```
- npm install 
```

### Step three. Creation of the required project structure.

#### Something like:
- src
   - config
      - connection_config.ts (configuration file for sequelize)
      - server.ts (contain jwt_secret)
   - db
      - migrations
      - models
      - seeders
   - logs
   - middlewares
   - order
      - controllers.ts
      - routes.ts
      - services.ts
   - static
   - test
   - user
      - controllers.ts
      - routes.ts
      - services.ts
   - app.ts

... and other required files

### Step four. Sequelize settings.
See documentation in [here](src/db/README.md)

### Step five. Added authorization.
See documentation in [here](src/middlewares/README.md)

### Step six. Logging user request in nameOfFile.log

### Step seven. Added docker multistage build.

### Step eight. Added simple test for testing CI/CD process.

#### For setting this process edit the configuration in the file: 

```
.gitlab-ci.yml
```

### Step nine. Added payment system.
See documentation in [here](src/order/README.md)

### Step nine. Added loading files.
